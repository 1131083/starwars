package com.example.pedro.starwars.menu.about

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.animation.Animation
import android.view.animation.RotateAnimation
import android.widget.ImageView
import com.example.pedro.starwars.R
import com.example.pedro.starwars.base.BaseActivity
import kotlinx.android.synthetic.main.toolbar_item.*


class AboutActivity : BaseActivity() {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, AboutActivity::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_about)
        showToolBarByKey(ABOUT_ACTIVITY)
        setSupportActionBar(toolbar)
        getSupportActionBar()!!.setDisplayHomeAsUpEnabled(true)

        startAnimation()
    }



    private fun startAnimation() {

        val rotate = RotateAnimation(
                0f, 360f,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f
        )
        rotate.duration = 800
        rotate.repeatCount = Animation.INFINITE
        findViewById<ImageView>(R.id.image_view).startAnimation(rotate)
    }

}
