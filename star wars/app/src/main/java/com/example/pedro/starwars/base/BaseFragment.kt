package com.example.pedro.starwars.base


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View


/**
 * A simple [Fragment] subclass.
 */
open class BaseFragment : Fragment() {


    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }
}
