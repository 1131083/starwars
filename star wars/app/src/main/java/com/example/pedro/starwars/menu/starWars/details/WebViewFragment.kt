package com.example.pedro.starwars.menu.starWars.details

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.pedro.starwars.R
import com.example.pedro.starwars.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_web_view.view.*


class WebViewFragment : BaseFragment() {

    companion object {
        const val URL_KEY: String = "URL"
        fun newIntent(url: String): Fragment {
            var fragment = WebViewFragment()
            var bundle = Bundle()
            bundle.putString(URL_KEY, url)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val mRootView = inflater!!.inflate(R.layout.fragment_web_view, container, false)

        activity.findViewById<Toolbar>(R.id.toolbar).visibility = View.GONE

        mRootView.web_view.getSettings().setJavaScriptEnabled(true);
        mRootView.web_view.getSettings().setLoadWithOverviewMode(true);
        mRootView.web_view.getSettings().setUseWideViewPort(true);
        mRootView.web_view.loadUrl("https://www.google.com/search?q=" + arguments.getString(URL_KEY));

        return mRootView;
    }


}