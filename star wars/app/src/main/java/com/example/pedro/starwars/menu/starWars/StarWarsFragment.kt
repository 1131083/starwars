package com.example.pedro.starwars.menu.starWars


import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.pedro.starwars.R
import com.example.pedro.starwars.`interface`.InterfaceRecyclerView
import com.example.pedro.starwars.adapters.CharacterPagerAdapter
import com.example.pedro.starwars.base.BaseActivity
import com.example.pedro.starwars.base.BaseFragment
import com.example.pedro.starwars.menu.starWars.details.StarWarsCharacterDetails
import com.example.pedro.starwars.model.Character
import kotlinx.android.synthetic.main.fragment_star_wars.view.*



class StarWarsFragment : BaseFragment(), InterfaceRecyclerView, StarWarsFragmentPresenter.View {

    private lateinit var mRootView: View

    private lateinit var recyclerView: RecyclerView


    private var presenter = StarWarsFragmentPresenter(this)


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        mRootView = inflater!!.inflate(R.layout.fragment_star_wars, container, false)
        recyclerView = mRootView.findViewById<RecyclerView>(R.id.recycler)

        presenter.init()

        return mRootView

    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()

    }

    override fun createRecyclerView(listCharacter: ArrayList<Character>) {

        if (recyclerView.adapter == null) {
            recyclerView.layoutManager = LinearLayoutManager(context)
            recyclerView.adapter = CharacterPagerAdapter(listCharacter, this)
        } else {
            recyclerView.adapter.notifyDataSetChanged()
        }

    }

    override fun askMore() {
        presenter.askMore()
    }

    override fun showLoading() {
        (activity as BaseActivity).showLoading()
    }

    override fun hideLoading() {
        (activity as BaseActivity).hideLoading()
    }

    override fun showError() {
        hideLoading()
        mRootView.tv_error.visibility = View.VISIBLE
    }

    override fun goToDetails(char: Character) {

        val intent = StarWarsCharacterDetails.newIntent(context, char)
        startActivity(intent)
    }

}
