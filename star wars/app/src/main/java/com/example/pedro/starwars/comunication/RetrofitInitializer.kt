package com.example.pedro.starwars.comunication

import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by pedro on 15/02/2019.
 */
class RetrofitInitializer {
    companion object {
        fun getRetrofit(): WikiApiService? {
            return Retrofit.Builder()

                    .baseUrl("https://swapi.co/api/")

                    .addConverterFactory(GsonConverterFactory.create())

                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())

                    .build().create(WikiApiService::class.java)

        }
    }

}