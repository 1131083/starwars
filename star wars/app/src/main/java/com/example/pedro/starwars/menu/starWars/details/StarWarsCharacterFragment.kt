package com.example.pedro.starwars.menu.starWars.details


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.pedro.starwars.R
import com.example.pedro.starwars.base.BaseActivity
import com.example.pedro.starwars.base.BaseFragment
import com.example.pedro.starwars.model.Character
import com.example.pedro.starwars.utils.Utils
import kotlinx.android.synthetic.main.fragment_star_wars_character.view.*
import kotlinx.android.synthetic.main.item_color_view.view.*
import kotlinx.android.synthetic.main.item_vehicle_layout.view.*


/**
 * A simple [Fragment] subclass.
 */
class StarWarsCharacterFragment : BaseFragment(), StarWarsDetailsFragmentPresenter.View {


    private lateinit var mRootView: View

    private var presenter = StarWarsDetailsFragmentPresenter(this)


    companion object {
        const val OBJECT_KEY: String = "OBJECT_CHAR"
        fun newIntent(char: Character): Fragment {
            val fragment = StarWarsCharacterFragment()
            val bundle = Bundle()
            bundle.putSerializable(OBJECT_KEY, char)
            fragment.arguments = bundle
            return fragment
        }
    }


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        mRootView = inflater!!.inflate(R.layout.fragment_star_wars_character, container, false)

        presenter.init(arguments.getSerializable(OBJECT_KEY) as Character)

        return mRootView;
    }

    override fun createCharacterInfo(character: Character) {
        mRootView.tv_name.text = Utils.getSpannableUnderLine(character.name)
        mRootView.tv_name.setOnClickListener({
            activity.supportFragmentManager.beginTransaction().add(R.id.main_content, WebViewFragment.newIntent(character.name)).addToBackStack(null).commit();
        })
        mRootView.tv_gender.text = character.gender
        mRootView.tv_planet.text = character.planet
        mRootView.tv_color.text = character.skinColor
    }

    override fun createCharacterColorLayout(character: Character) {
        var view: View
        character.getSkinsColor().forEach {
            view = LayoutInflater.from(context).inflate(R.layout.item_color_view, null, false)
            view.view_color.setBackgroundResource(context.resIdByName(it.replace(" ", "").replace(" ", "_"), "color"))
            mRootView.linear_color.addView(view)
        }
    }

    override fun createVehicleLayout(character: Character) {
        var view: View
        character.vehicle.forEach {
            view = LayoutInflater.from(context).inflate(R.layout.item_vehicle_layout, null, false)

            view.tv_vehicle_name.text = Utils.getSpannableUnderLine(it.name)
            val vName = it.name
            view.tv_vehicle_name.setOnClickListener({
                activity.supportFragmentManager.beginTransaction().add(R.id.main_content, WebViewFragment.newIntent(vName)).addToBackStack(null).commit();

            })
            view.tv_vehicle_model.text = it.model
            view.tv_vehicle_speed.text = getString(R.string.car_max_speed, it.maxAtmospheringSpeed)
            mRootView.linear_container.addView(view)
        }
    }


    override fun showLoading() {
        (activity as BaseActivity).showLoading()
    }

    override fun hideLoading() {
        (activity as BaseActivity).hideLoading()
    }


   private fun Context.resIdByName(resIdName: String?, resType: String): Int {
        resIdName?.let {
            return resources.getIdentifier(it, resType, packageName)
        }
        return R.color.black
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()

    }
}// Required empty public constructor
