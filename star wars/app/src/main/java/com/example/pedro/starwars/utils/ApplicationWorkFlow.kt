package com.example.pedro.starwars.utils

import android.support.v4.app.Fragment
import com.example.pedro.starwars.base.BaseActivity
import com.example.pedro.starwars.R

/**
 * Created by pedro on 15/02/2019.
 */


class ApplicationWorkFlow {


    companion object {
        fun switchFragment(activity: BaseActivity, fragment:Fragment) {

            activity.supportFragmentManager.beginTransaction().add(R.id.main_content, fragment).commit();
        }
    }
}