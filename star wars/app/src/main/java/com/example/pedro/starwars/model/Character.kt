package com.example.pedro.starwars.model

import java.io.Serializable

/**
 * Created by pedro on 15/02/2019.
 */
data class Character(val id: String, val name: String, val nVehicles: Int, val gender: String, val skinColor: String) : Serializable {
    var specie: String = "Unknown"
    var planet: String = "Unknown"
    var vehicle: ArrayList<Vehicle> = ArrayList()

    fun getSkinsColor(): List<String> {

        return skinColor.split(",")


    }

}