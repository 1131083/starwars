package com.example.pedro.starwars.`interface`

import com.example.pedro.starwars.model.Character

/**
 * Created by pedro on 15/02/2019.
 */
interface InterfaceRecyclerView {

    fun askMore()
    fun goToDetails(char: Character)
}