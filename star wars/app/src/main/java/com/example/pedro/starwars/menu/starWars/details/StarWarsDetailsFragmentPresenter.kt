package com.example.pedro.starwars.menu.starWars.details

import com.example.pedro.starwars.model.Character

/**
 * Created by pedro on 16/02/2019.
 */
class StarWarsDetailsFragmentPresenter(var view: StarWarsCharacterFragment?) {

    var characterMV: Character? = null

    interface View {
        fun showLoading()
        fun hideLoading()
        fun createCharacterInfo(character: Character);
        fun createCharacterColorLayout(character: Character);
        fun createVehicleLayout(character: Character);

    }

    fun init(character: Character) {

        if (character != null) {
            characterMV = character
            view!!.createCharacterInfo(character)
            view!!.createCharacterColorLayout(character)
            view!!.createVehicleLayout(character)
        }
    }


    fun onDestroy() {
        view = null
    }

}