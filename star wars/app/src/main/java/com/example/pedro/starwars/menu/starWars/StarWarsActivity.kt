package com.example.pedro.starwars.menu.starWars

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.example.pedro.starwars.R
import com.example.pedro.starwars.base.BaseActivity
import com.example.pedro.starwars.utils.ApplicationWorkFlow
import kotlinx.android.synthetic.main.toolbar_item.*

class StarWarsActivity : BaseActivity() {

    companion object {

        fun newIntent(context: Context): Intent {
            val intent = Intent(context, StarWarsActivity::class.java)
            return intent
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_star_wars)
        setSupportActionBar(toolbar)
        getSupportActionBar()!!.setDisplayHomeAsUpEnabled(true)

        showToolBarByKey(BaseActivity.LIST_STAR_WARS_ACTIVITY)
        ApplicationWorkFlow.switchFragment(this,StarWarsFragment())

    }

}
