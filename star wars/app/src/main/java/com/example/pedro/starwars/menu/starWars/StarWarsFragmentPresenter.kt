package com.example.pedro.starwars.menu.starWars

import com.example.pedro.starwars.comunication.RetrofitInitializer
import com.example.pedro.starwars.comunication.response.ResponseCharacter
import com.example.pedro.starwars.model.Character
import com.example.pedro.starwars.model.Vehicle
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

/**
 * Created by pedro on 16/02/2019.
 */
class StarWarsFragmentPresenter(var view: View?) {

    private var myCompositeDisposable: CompositeDisposable? = null
    private var nRequests = 1
    private var loadingControl = -1
    private var maxSize = 0;

    private val listCharacter: ArrayList<Character> = ArrayList()

    interface View {
        fun showLoading()
        fun hideLoading()
        fun createRecyclerView(list: ArrayList<Character>)
        fun showError()

    }

    fun init() {
        myCompositeDisposable = CompositeDisposable()
        view!!.showLoading()
        loadData()
    }

    private fun loadData() {

        myCompositeDisposable?.add(RetrofitInitializer.getRetrofit()!!.list(nRequests)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse, this::handlerError))
    }

    private fun handleResponse(response: ResponseCharacter) {

        maxSize = response.count

        if (response.next != null && response.next.length != null && response.next.isNotEmpty()) {
            nRequests += 1
        }
        response.results.forEach {
            val character = Character(it.url, it.name, it.vehicles.size, it.gender, it.skin_color)

            it.species.forEach {
                loadingControl += 1
                requestSpecies(it, character);
            }
            it.vehicles.forEach {
                loadingControl += 1
                requestVehicles(it, character)
            }
            loadingControl += 1
            requestPlanets(it.homeworld, character)

            listCharacter.add(character)
        }

    }

    private fun handlerError(error: Throwable) {
        view!!.showError()
    }

    private fun requestSpecies(url: String, character: Character) {

        myCompositeDisposable?.add(RetrofitInitializer.getRetrofit()!!.getSpecies(url)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    hideLoading()
                    if (it != null) {
                        character.specie = it.name
                    }
                }, { view!!.hideLoading() }))
    }

    private fun requestPlanets(url: String, character: Character) {

        myCompositeDisposable?.add(RetrofitInitializer.getRetrofit()!!.getPlanets(url)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({

                    hideLoading()
                    if (it != null) {
                        character.planet = it.name
                    }
                }, {   view!!.showError() }))
    }

    private fun requestVehicles(url: String, character: Character) {

        myCompositeDisposable?.add(RetrofitInitializer.getRetrofit()!!.getVehicles(url)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    hideLoading()
                    if (it != null) {
                        character.vehicle.add(Vehicle(it.url, it.name, it.model, it.vehicle_class, it.max_atmosphering_speed))
                    }
                }, {   view!!.showError() }))
    }

    fun hideLoading() {
        if (loadingControl == 0) {
            view!!.hideLoading()
            view!!.createRecyclerView(listCharacter)
        } else
            loadingControl -= 1
    }

    fun askMore() {
        if (listCharacter.size <= maxSize) {
            loadData()
            loadingControl = -1
            view!!.showLoading()

        }
    }

    fun onDestroy() {
        view = null
    }

}


