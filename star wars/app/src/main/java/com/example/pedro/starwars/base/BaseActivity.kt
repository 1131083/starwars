package com.example.pedro.starwars.base

import android.app.ProgressDialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.widget.TextView
import com.example.pedro.starwars.R
import com.example.pedro.starwars.comunication.RetrofitInitializer


open class BaseActivity : AppCompatActivity() {

    companion object {
        const val MENU_ACTIVITY = "MENU"
        const val LIST_STAR_WARS_ACTIVITY = "list_stars"
        const val DETAILS_STAR_WARS_ACTIVITY = "details"
        const val ABOUT_ACTIVITY = "about"
    }

    private lateinit var progressDialog: ProgressDialog


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        RetrofitInitializer()
        progressDialog = ProgressDialog(this)
    }

    fun showToolBarByKey(id: String) {
        val tvTitle = findViewById<TextView>(R.id.tv_toolbar_text)

        when (id) {
            MENU_ACTIVITY -> tvTitle.text = getString(R.string.toolbar_menu)
            LIST_STAR_WARS_ACTIVITY -> tvTitle.text = getString(R.string.toolbar_list)
            DETAILS_STAR_WARS_ACTIVITY -> tvTitle.text = getString(R.string.toolbar_details)
            ABOUT_ACTIVITY -> tvTitle.text = getString(R.string.title_activity_about)
            else -> {
                tvTitle.text = ""
            }
        }

    }

    fun showLoading() {
        progressDialog.setTitle("Loading")
        progressDialog.setCancelable(false);
        progressDialog.show()
    }

    fun hideLoading() {

        progressDialog.dismiss()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            android.R.id.home -> {
                // app icon in action bar clicked; goto parent activity.
                this.finish()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }
}
