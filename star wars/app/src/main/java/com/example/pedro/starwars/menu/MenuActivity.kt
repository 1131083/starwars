package com.example.pedro.starwars.menu

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.example.pedro.starwars.R
import com.example.pedro.starwars.base.BaseActivity
import com.example.pedro.starwars.utils.ApplicationWorkFlow
import kotlinx.android.synthetic.main.toolbar_item.*

class MenuActivity : BaseActivity() {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, MenuActivity::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)
        setSupportActionBar(toolbar)
        showToolBarByKey(BaseActivity.MENU_ACTIVITY)

        ApplicationWorkFlow.switchFragment(this, MenuFragment.newInstance())
    }

}
