package com.example.pedro.starwars.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.pedro.starwars.R
import com.example.pedro.starwars.`interface`.InterfaceRecyclerView
import com.example.pedro.starwars.model.Character
import kotlinx.android.synthetic.main.character_item_recycler_view.view.*

/**
 * Created by pedro on 15/02/2019.
 */
class CharacterPagerAdapter(val items: ArrayList<Character>, val interfaceRecyclerView: InterfaceRecyclerView) : RecyclerView.Adapter<ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent?.context).inflate(R.layout.character_item_recycler_view, parent, false));
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        holder?.tvName?.text = items.get(position).name
        holder?.tvSpecies?.text = items.get(position).specie
        holder?.tvNumber?.text = holder!!.itemView.context.getString(R.string.vehicle_number,items.get(position).nVehicles.toString())

        if (position == items.size - 1) {
            interfaceRecyclerView.askMore()
        }
        holder.itemView?.setOnClickListener(View.OnClickListener { interfaceRecyclerView.goToDetails(items.get(position)) })
    }


}

class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val tvName = view.tv_name
    val tvSpecies = view.tv_specie
    val tvNumber = view.tv_number

}