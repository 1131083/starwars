package com.example.pedro.starwars.menu.starWars.details


import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.view.View
import com.example.pedro.starwars.R
import com.example.pedro.starwars.base.BaseActivity
import com.example.pedro.starwars.model.Character
import com.example.pedro.starwars.utils.ApplicationWorkFlow
import kotlinx.android.synthetic.main.toolbar_item.*

class StarWarsCharacterDetails : BaseActivity() {

    private lateinit var character: Character

    companion object {
        const val OBJECT_KEY: String = "OBJECT_CHAR"
        fun newIntent(context: Context, char: Character): Intent {
            val intent = Intent(context, StarWarsCharacterDetails::class.java)
            intent.putExtra(OBJECT_KEY, char)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_star_wars_character_details)
        setSupportActionBar(toolbar)
        getSupportActionBar()!!.setDisplayHomeAsUpEnabled(true)
        showToolBarByKey(BaseActivity.DETAILS_STAR_WARS_ACTIVITY)

        if (intent.hasExtra(OBJECT_KEY)) {
            character = (intent.getSerializableExtra(OBJECT_KEY) as Character?)!!
        }
        ApplicationWorkFlow.switchFragment(this, StarWarsCharacterFragment.newIntent(character))

    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (findViewById<Toolbar>(R.id.toolbar) != null && findViewById<Toolbar>(R.id.toolbar).visibility == View.GONE)
                findViewById < Toolbar >(R.id.toolbar).visibility = View.VISIBLE
    }
}
