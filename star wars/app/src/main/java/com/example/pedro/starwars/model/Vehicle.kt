package com.example.pedro.starwars.model

import java.io.Serializable

/**
 * Created by pedro on 15/02/2019.
 */
data class Vehicle(val id: String, val name: String, val model: String, val vehicleClass: String, val maxAtmospheringSpeed: String) : Serializable {
}