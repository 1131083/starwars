package com.example.pedro.starwars.comunication


import com.example.pedro.starwars.comunication.response.ResponseCharacter
import com.example.pedro.starwars.comunication.response.ResponsePlanets
import com.example.pedro.starwars.comunication.response.ResponseSpecies
import com.example.pedro.starwars.comunication.response.ResponseVehicles
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

/**
 * Created by pedro on 15/02/2019.
 */
interface WikiApiService {
    @GET("people/")
    fun list(@Query("page") page: Int): Observable<ResponseCharacter>

    @GET
    fun getSpecies(@Url url: String): Observable<ResponseSpecies>

    @GET
    fun getPlanets(@Url url: String): Observable<ResponsePlanets>

    @GET
    fun getVehicles(@Url url: String): Observable<ResponseVehicles>


}

