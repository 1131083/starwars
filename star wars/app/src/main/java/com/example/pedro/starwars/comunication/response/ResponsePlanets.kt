package com.example.pedro.starwars.comunication.response

/**
 * Created by pedro on 15/02/2019.
 */
data class ResponsePlanets(
    val climate: String,
    val created: String,
    val diameter: String,
    val edited: String,
    val films: List<String>,
    val gravity: String,
    val name: String,
    val orbital_period: String,
    val population: String,
    val residents: List<String>,
    val rotation_period: String,
    val surface_water: String,
    val terrain: String,
    val url: String
)