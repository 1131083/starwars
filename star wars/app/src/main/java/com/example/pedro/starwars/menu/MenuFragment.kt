package com.example.pedro.starwars.menu

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.example.pedro.starwars.R
import com.example.pedro.starwars.base.BaseFragment
import com.example.pedro.starwars.menu.about.AboutActivity
import com.example.pedro.starwars.menu.starWars.StarWarsActivity


class MenuFragment : BaseFragment() {


    companion object {
        fun newInstance(): MenuFragment {

            return MenuFragment()
        }
    }

    val clickStartWars = View.OnClickListener { view ->
        startActivity(StarWarsActivity.newIntent(context))
    }
    val clickAbout = View.OnClickListener { view ->
        startActivity(AboutActivity.newIntent(context))
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val mRootView = inflater!!.inflate(R.layout.fragment_menu, container, false)
        val btnSWars = mRootView.findViewById<Button>(R.id.btn_star_people)
        val btnAbout = mRootView.findViewById<Button>(R.id.btn_about)

        btnSWars.setOnClickListener(clickStartWars);
        btnAbout.setOnClickListener(clickAbout);
        return mRootView
    }


}
