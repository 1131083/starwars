package com.example.pedro.starwars.utils

import android.text.SpannableString
import android.text.style.UnderlineSpan

/**
 * Created by pedro on 17/02/2019.
 */
class Utils {
    companion object {
        fun getSpannableUnderLine(text:String): SpannableString {
            val content = SpannableString(text)
            content.setSpan(UnderlineSpan(), 0, content.length, 0)
            return content
        }
    }
}